<?php
    $DB_NAME = "kampus";
    $DB_USER = "root";
    $DB_PASS =  "";
    $DB_SERVER_LOC = "localhost";

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
        $sql = "SELECT jns_kelamin FROM jenis_kelamin ORDER BY jns_kelamin asc";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            header("Access-Control-Allow-Origin: *");
            header("Content-type: application/json; charset=UTF-8");

            $jns_kelamin = array();
            while($nm_kelamin = mysqli_fetch_assoc($result)){
                array_push($jns_kelamin,$nm_kelamin);
            }
            echo json_encode($jns_kelamin);
        }
    }
?>